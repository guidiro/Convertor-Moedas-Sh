#!/bin/bash

URL="https://api.exchangeratesapi.io/latest?base="$1"&symbols="$2""

valor=`curl $URL | jq .rates.$2`

valorresultado=$(python -c "print $valor*$3")

echo "Moeda Base: $1"
echo "Moeda para Converter: $2" 
echo "Valor Convertido: " $valorresultado
